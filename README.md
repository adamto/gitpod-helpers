Set of files for developing with gitpod

# Adminer
DB management tool with custom css file.  
`index.php` is modified to set user and password on login page to `root`
```
'<input name="auth[username]" id="username" value="'.h("root").'" autocomplete="username" autocapitalize="off">
'<input type="password" name="auth[password]" autocomplete="current-password" value="root">'
```

# DOTENV
Script setting ini config variabled for files like `.env`  
Taken from https://github.com/bashup/dotenv  
Update by just downloading the https://github.com/bashup/dotenv/blob/master/dotenv file

# EXPOSE (https://expose.dev)
App exposing your local ports to the world.  
Use `expose` for PHP7 and `expose2` for PHP8. (`expose` may also be version2 but older, no idea)  
Update by running steps from https://expose.dev/docs/getting-started/installation
```
curl https://github.com/beyondcode/expose/raw/master/builds/expose -L --output expose

chmod +x expose
```
